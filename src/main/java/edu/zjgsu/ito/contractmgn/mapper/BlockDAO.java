package edu.zjgsu.ito.contractmgn.mapper;

import edu.zjgsu.ito.contractmgn.model.Block;
import edu.zjgsu.ito.contractmgn.model.BlockExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * BlockDAO继承基类
 */
@Repository
public interface BlockDAO extends MyBatisBaseDao<Block, Integer, BlockExample> {


    @Select("select * from block where id=#{blockId}")
    Block selectBlockById(@Param("blockId") Integer blockId);

    @Select("select block_id from block_user where user_id = #{value} ")
	Integer getBlockById(Integer attribute);

    @Select("select block_id from block_user where operate_id = #{value} ")
    Integer getBlockByOperatorId(Integer attribute);

    /**
    * @Author 陈荣锵
    * @Description //TODO asus 得到所有的区块
    * @Date 4:19 PM 4/11/2020
    * @Param []
    * @return java.util.List<edu.zjgsu.ito.contractmgn.model.Block>
    **/
    @Select("SELECT * FROM block")
    List<Block> getAllBlock();

    @Select("SELECT COUNT(*) FROM block")
    Integer getCount();

}