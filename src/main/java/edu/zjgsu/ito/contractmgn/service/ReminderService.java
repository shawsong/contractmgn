package edu.zjgsu.ito.contractmgn.service;

import edu.zjgsu.ito.contractmgn.pojo.Message;

public interface ReminderService {

    Message getReminder(int type,int pageNum,int record);

    Message readReminderInContract(int id);

    Message readAllReminder(int type);

    void insertReminder(int contractId,int type,String operateType);

    Message countUnreadReminder();

    Message readSingleReminder(int id);

}
