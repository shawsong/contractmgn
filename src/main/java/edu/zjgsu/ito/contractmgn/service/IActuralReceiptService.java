package edu.zjgsu.ito.contractmgn.service;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import edu.zjgsu.ito.contractmgn.model.ActuralReceiptList;
import edu.zjgsu.ito.contractmgn.model.ActuralTicket;
import edu.zjgsu.ito.contractmgn.model.ReceiptModel;
import edu.zjgsu.ito.contractmgn.pojo.Message;

import javax.servlet.http.HttpServletResponse;

/**
 * IActuralReceiptService
 */
public interface IActuralReceiptService {
	void exportTotal(
						HttpServletResponse response,
						String headName,
						Integer blockId,
						String startDate,
					  	String endDate,
					 	String changeStartDate,
					 	String changeEndDate
	) throws IOException;

	Message deleteActualTicket(Integer actualticketid);//min,删除实开票;

	Integer deleteReceipt(Integer actrualId);//删除实收款，min

	ActuralReceiptList getReceiptList(Integer id);

	Double isPlan(Integer contractId);

	Integer insertReceipt(ReceiptModel model,Integer opratorTag);

	List<Map<String, Object>> getAcuralTicket(Integer id);

	Integer addActralTicket(ActuralTicket model);

	Double isPlanTicket(Integer contractId);

	List<Map<String, Object>> getTicketList(String key, Date startDate, Date endDate, Integer blockId);

	List<Map<String, Object>> getTicketDetail(Integer contractId);

	List<Map<String, Object>> getActralDetail(Integer id);

	List<LinkedHashMap<String, Object>> getOutput(Integer blockId, Integer contractTypeId, String sql, String startDate,
			String endDate, String changeStartDate, String changeEndDate);

	List<Map<String, Object>> getNodeList(Integer contractId);

}